exports.install = () => {
    F.route('/', view_list)
    F.route('/{id}', view_article)
    F.websocket('/{id}', add_comment_asynchronous)
    F.route('/article', refresh_article)
}

function refresh_article(){
    MODEL('article').save("http://blogforcdntest.prismic.io/api")
}

function view_list(){
    let pagination = {
        'page' : 1,
        'maxPage' : 0
    }

    if (this.get.page !== undefined){
        pagination.page = Number(this.get.page)
    }
    MODEL('article').article_pagination(pagination, (err, data) => {
        if (err) throw err

        this.view('index', {
            articles: data,
            size : Array(pagination.maxPage)
        })
    })
}

function view_article(id) {
    var article = {
        id : id
    }
    article.commentMax = 3
    if (this.get.commentMax !== undefined){
        article.commentMax = parseInt(this.get.commentMax)
    }
    MODEL('article').read(article, (err, document) => {
        let data = !document  ? {} : document[0]

        let comments = data.comments
        if (!comments[0].author){
            data.comments = []
            data.sizeComments = 0
        }
        this.view('article', {
            article : data
        })
    })
}

function add_comment_asynchronous(id){
    let self = this

    self.on('open', (client) => {
        console.log('connected ?')
    })

    self.on('close', (client) => {
        console.log('close ?')
    })

    self.on('message', (client, message) => {
        let message_json = JSON.parse(message)
        message_json.date_comment = new Date()
        let comment = GETSCHEMA('Comment').make(message_json, (err, model) => {
                let errors = {}
                let map = {
                    'author' : 'Auteur : ',
                    'content' : 'Commentaire : '
                }

                if (err){
                    err.items.forEach((item) => {
                        errors[item.name] = map[item.name] + "Champ invalide "
                    })
                }
                if (model){
                    model.idArticle = id
                    model.$save((err, document) => {
                    })
                }
                self.send(JSON.stringify(errors))
        })
    })
}
