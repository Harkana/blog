/**
 * Created by arkanzel on 03/12/16.
 */

(function($){
    $("button#btn_page").on('click', function(event){
       var target = event.target;
       var attrs = $(target).attr('data-page');

        $.get('/?page=' + parseInt(attrs), function(data){
            var indexBegin = data.indexOf('<body');
            var indexEnd = data.indexOf('</body>') + 7;
            var new_data = data.slice(indexBegin, indexEnd);
            $('body').html(new_data);
        });
    });
})(jQuery);

