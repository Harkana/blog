/**
 * Created by arkanzel on 03/12/16.
 */
(function ($) {
    var section = $("section#list_commentaire");
    var id = section.attr('data-id');
    var cPage = section.attr('data-page');
    if (!cPage) {
        cPage = 0;
    }
    var HOST = location.origin.replace(/^http/, 'ws');
    var ws = new WebSocket(HOST + "/" + id);

    if (!cPage) {
        cPage = 3;
    }

    (function () {
        var tmpData = "";

        function isEmpty(str) {
            console.log(str);
            if (!str)
                return (true);
            return (str.trim().length == 0);
        }

        function refreshUIComment(data) {
            var indexBegin = data.indexOf("<article data-begin");
            var indexEnd = data.indexOf("<footer data-end");
            var newData = data.slice(indexBegin, indexEnd);
            if (newData.localeCompare(tmpData)) {
                $("article#container_commentaire").html(newData);
            }
            tmpData = newData;
            moreComment();
        }

        function refreshComment() {
            $.get('/' + id + '?commentMax=' + cPage, function (data) {
                refreshUIComment(data);
            });
        }

        function moreComment() {
            $("button#more_comment").on('click', function (event) {
                event.preventDefault();

                cPage += 3;
                $.get('/' + id + '?commentMax=' + cPage, function (data) {
                    refreshUIComment(data);
                });
            });
        }

        function save_com() {

            ws.onopen = function () {
                console.log("web socket open");
            };


            ws.onclose = function () {
                console.log("web socket close");
            };

            ws.onmessage = function (e) {
                var str = decodeURIComponent(e.data);
                var ret_json = JSON.parse(str);
                let message = "";

                for (var key in ret_json) {
                    message += ret_json[key];
                    message += "\n";
                }
                if (message.length) {
                    alert(message);
                }
                refreshComment();
            };

            $("#on_submit_comment").on('click', function (event) {
                event.preventDefault();
                let author = $("input#author").val();
                let content = $("textarea#content").val();

                var str = JSON.stringify({
                    author: author,
                    content: content
                });
                ws.send(encodeURIComponent(str));
                $("input#author").val("");
                $("textarea#content").val("");
            });
        }

        moreComment();
        save_com();
    })();
})(jQuery);