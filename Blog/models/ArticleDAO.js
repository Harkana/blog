/**
 * Created by arkanzel on 02/12/16.
 */

const ObjectID = require('mongodb').ObjectID
let Prismic = require('prismic.io')
const ARTICLE_PER_PAGE = 5

exports.id = "article"

exports.save = (str, ft) => {
    Prismic.api(str, function(err, api){
        let array_article = []

        api.query("", {}, function (error, response) {
            if (error) throw error

            let results = response.results
            results.forEach((list) => {
                let data = list.data
                let rowState = null

                if (data['blogtype.rowState']){
                    rowState = data['blogtype.rowState']['value']
                }
                let to_find = {
                    'uid_prismic' : list['uid']
                }
                let article = {
                    'title' : data['blogtype.title']["value"][0]["text"],
                    'author' : data['blogtype.author']["value"][0]["text"],
                    'content' : data['blogtype.content']['value'],
                    'uid_prismic' : list['uid']
                }
                DB('blog').findOne(to_find,
                    {
                        "_id" : 1
                    } , (err, res) => {
                        if (res == null && (!rowState || rowState == "S")){
                            article['date_article'] = new Date()
                            DB('blog').save(article)
                        }
                        if (rowState == "D" && res != null){
                            DB('blog').remove(res)
                        }
                        if (rowState == "U" && res != null){
                            article['date_article'] = new Date()
                            DB('blog').update(res, {
                                "$set" : {
                                        "title" : article.title,
                                        "author" : article.author,
                                        "content" : article.content,
                                        "date_article" : article.date_article
                                    }
                                }
                            )
                        }
                })
            })
        })
    })
}

exports.updateArticle = (id, comment, ft) => {
    DB('blog').update({
        '_id': new ObjectID(id)
    }, {
        '$push': {
            comments: comment
        }
    }, ft)
}

exports.article_pagination = (pagination, ft) => {
    DB('blog').find({}, {
        "_id" : 1
    }).count((err, data) => {
        let page = pagination.page - 1
        pagination.maxPage = Math.ceil(data / ARTICLE_PER_PAGE)
        if (page >= data){
            page = (data - ARTICLE_PER_PAGE)
        }
        if (page < 0)
            page = 0
        if (data == 0 || page < data){
            DB('blog').aggregate([
                {
                    '$project' : {
                        "author": 1,
                        "title": 1,
                        "content": 1,
                        "date_article" : 1,
                        "_id" : 1,
                        "sizeComments" : {
                            "$size" : {
                                "$ifNull" : [
                                    "$comments", []
                                ]
                            }
                        }
                    }
                },
                {
                    "$sort" : {
                        "_id" : -1
                    }
                },
                {
                    "$skip" : page * ARTICLE_PER_PAGE
                },
                {
                    "$limit" : ARTICLE_PER_PAGE
                }
            ], ft)
        }
    })
}

exports.read = (article, ft) => {
    DB('blog').aggregate([
        {
           '$match' : {
               '_id' :   new ObjectID(article.id)
           }

        },
        {
          '$unwind' : {
              path : "$comments",
              preserveNullAndEmptyArrays : true
          }
        },
        {
            '$sort' : {
                'comments.date_comment' : -1
            }
        },
        {
            '$group' : {
                '_id' : {
                    "_id" : "$_id",
                    "title" : "$title",
                    "author" : "$author",
                    "content" :"$content",
                    "date_article" : "$date_article"
                },
                'comments' : {
                    '$push' : {
                        "date_comment" : "$comments.date_comment",
                        "author": "$comments.author",
                        "content" : "$comments.content"
                    }
                }
            }
        },
        {
            '$project' : {
                "_id" : "$_id._id",
                "title" :"$_id.title",
                "author" : "$_id.author",
                "content" : "$_id.content",
                "date_article":"$_id.date_article",
                "comments":{
                    "$slice" : [
                        "$comments",
                        article.commentMax
                    ]
                },
                "sizeComments" : {
                    "$size" : "$comments"
                }
            }
        }
    ], ft)
}

NEWSCHEMA('Comment').make((schema) => {
    schema.define('author', 'String(30)', true)
    schema.define('date_comment', 'Date', false)
    schema.define('content', 'String(255)', true)

    schema.setValidate((name, value) => {
        let str = value.trim()

        switch (name){
            case 'author' :
                return (str.length > 0 && str.length < 30)
            break
            case 'content' :
                return  (str.length > 0 && str.length < 255)
            break
        }
    })

    schema.setSave((error, model, options, callback) => {
        let id = model.idArticle
        let fields = { 'author' : 1, 'content' : 1, 'date_comment' : 1}
        let newModel = {}

        for (var key in model){
            if (fields[key] == 1){
                newModel[key] = model[key]
            }
        }
        MODEL('article').updateArticle(id, newModel, callback)
    })
})

NEWSCHEMA('Article').make((schema) => {
    schema.define('author', 'String(30)', true)
    schema.define('title', 'String(50)', true)
    schema.define('date_article', 'Date', true)
    schema.define('content', 'String(255)', true)

    schema.setValidate((name, value) => {
        switch (name){
            case 'author' :
                return value && value.length > 0 && value < 30
            case 'title' :
                return value && value.length > 0 && value.length < 50
            case 'content' :
                return value && value.length > 0 && value.length < 255
        }
    })

    schema.setSave((error, model, options, callback) => {
        DB('blog').save(model)
        callback(SUCCESS(true))
    })
})